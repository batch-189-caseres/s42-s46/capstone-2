const Product = require('../models/Product');
const User = require('../models/User');



module.exports.addProduct = (reqBody, userData) => {

    return User.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {
            return "You are not an admin"
        } else {
            let newProduct = new Product({
                name: reqBody.name,
                description: reqBody.description,
                imgUrl: reqBody.imgUrl,
                category: reqBody.category,
                price: reqBody.price,
                stocks: reqBody.stocks
            })
        	console.log(newProduct)
            return newProduct.save().then((product, error) => { 
                if(error) {
                    return false
                } else {
                    return true
                }
            })
        }
        
    });    
}

module.exports.getAllActive = () => {

	return Product.find({isActive: true, onSale: false}).then(result => {

		return result
	})

}

module.exports.getAllOnSale = () => {

	return Product.find({isActive: true, onSale: true}).then(result => {

		return result
	})

}

module.exports.getAllProduct = () => {
	return Product.find().then(result => {
		return result
	})
}

module.exports.getProduct = (reqParams) => {


	return Product.findById(reqParams.productId).then(result => {

		return result
	})

}

module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {

		name: reqBody.name,
		description: reqBody.description,
		category: reqBody.category,
		price: reqBody.price,
		imgUrl: reqBody.imgUrl,
		stocks: reqBody.stocks	

	}

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) =>{

		
		if(error) {

			return false

		} else {

			return true
		
		}


	})


}

module.exports.archiveProduct = (reqParams, reqBody) => {

	let archivedProduct = {

		isActive: reqBody.isActive
	}

	return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product, error) =>{
		
		if(error) {

			return false

		} else {

			if(reqBody.isActive == false){
			  return false
			} else {
				return true
			}
		
		}
	})
}


module.exports.setOnSale = (reqParams, reqBody) => {

	let setSale = {

		onSale: reqBody.onSale
	}

	return Product.findByIdAndUpdate(reqParams.productId, setSale).then((product, error) =>{
		
		if(error) {

			return false

		} else {

			if(reqBody.onSale == false){
			  return false
			} else {
				return true
			}
		
		}
	})
}

module.exports.removeProduct = (reqParams) => {

	return Product.findByIdAndDelete(reqParams.productId).then((result, error) => {
        if(result != null){
        	return true
        } else {
        	return false
        }
    })
}