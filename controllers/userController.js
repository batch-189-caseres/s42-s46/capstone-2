const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Product = require('../models/Product');

//Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {

    // The result is sent back to the frontend via the "then" method found in the route file
    return User.find({email: reqBody.email}).then(result => {

        // The "find" method returns a record if a match is found
        if (result.length > 0) {
            return true

        // No duplicate email found
        // The user is not yet registered in the database
        } else {
            return false
        }

    })

}


module.exports.registerUser = (reqBody) => {

    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10) 

    })

    return newUser.save().then((user, error) => {

// User registration failed
if(error) {
    return false

// User registration successful
} else {
    return true
}

})
}
module.exports.loginUser = (reqBody) => {


    return User.findOne({email: reqBody.email}).then(result => {

        if (result == null) {
            return false
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if(isPasswordCorrect) {
                return { access: auth.createAccessToken(result)}
            } else {
                return false
            }
        }

    })
}

module.exports.checkBalance = (userId) => {
    return User.findById(userId).then(result => {
       return `${result.balance}`
    })
}

module.exports.cashIn = (data, reqBody) => {
    return User.findById(data.userId).then(result => {
        let newBalance = result.balance + reqBody.balance
        let updatedBalance = {
        balance: newBalance
    }
    return User.findByIdAndUpdate(data.userId, updatedBalance).then((balance, error) => {
        if(balance == null || balance == undefined){
            return false
        } else {
            return `${updatedBalance.balance}`
        }
    })
    })
    
}


module.exports.getProfile = (data) => {

    return User.findById(data.id).then(result => {
        result.password = "";      
        return result;

    });

}

module.exports.getAdminProfile = (data) => {

    return User.findById(data.userId).then(result => {
        result.password = "";      
        return result;

    });

}

module.exports.setAdmin = (reqParams, reqBody) => {


    let setAdmin = {

        isAdmin: reqBody.isAdmin
    }

    return User.findByIdAndUpdate(reqParams.userId, setAdmin).then((user, error) =>{
        if(error) {

            return false


        } else {

            if(reqBody.isAdmin == false){
                return false
            } else {
                return true
            }

        }


    })
}

module.exports.checkCart = (userId) => {
    return User.findById(userId).then(result => {
        if(result.addToCart.length == 0){
            return false
        } else {
            return result.addToCart
        }
    })
}

module.exports.itemCheckCart = (userId ,reqParams) => {
    return User.findById(userId).then(result => {
        if(result.addToCart.length === 0){
            return false
        } else {
            for(let i = 0; i < result.addToCart.length; i++){
            if(result.addToCart[i].productId === reqParams.productId){
                return true
            } else if ( result.addToCart.length - i == 1) {
                return false
            } else {
                continue;
            }
         }
        }
    })
}

module.exports.addItemQuantity = (userId, reqParams) => {
    return User.findById(userId).then(result => {
        for(let i = 0; i < result.addToCart.length; i++){
            if(result.addToCart[i].productId == reqParams.productId){
                result.addToCart[i].quantity++

                return result.save().then((user, error) => {
                    if(error) {
                        return false
                    } else {
                        return true
                    }
                })
            } else {
                continue;
            }
        }
    })
} 

module.exports.updatedQuantity = (userId, reqParams, quantity) => {
    return User.findById(userId).then(result => {
        for(let i = 0; i < result.addToCart.length; i++){
            if(result.addToCart[i].productId == reqParams.productId){
                result.addToCart[i].quantity = quantity

                return result.save().then((user, error) => {
                    if(error) {
                        return false
                    } else {
                        return true
                    }
                })
            } else {
                continue;
            }
        }
    })
} 

module.exports.addToCart = (data) => {
    return User.findById(data.userId).then(result => {
         return  Product.findById(data.productId).then(product => {
            const { name, description, imgUrl, category   } = product
            result.addToCart.push({productId: data.productId, name: name, description: description, imgUrl: imgUrl, category: category, quantity: data.quantity, totalAmount: data.totalAmount})
        return result.save().then((user, error) => {
           if(error) {
                return false
            } else {
                return true
            } 
        })
        })
        
    })
}



module.exports.removeToCart = (data) => {
    return User.findById(data.userId).then(user => {
        if(user.addToCart.length === 1){
             user.addToCart.splice(0)
            return user.save().then((user, error) => {
            if(error) {
                return false
            } else {
                return true
            }
            })

        } else {
            for( let i = 0; i < user.addToCart.length; i++) {
            if(user.addToCart[i].productId === data.productId){
                user.addToCart.splice(i, 1)
            return user.save().then((user, error) => {
           
            if(error) {
                return false
            } else {
                return true
            }
            })
            } else {
                continue;
            }
        }
        }
    })
}

module.exports.subtotal = (data) => {
    return User.findById(data.userId).then(result => {
        if(result.addToCart.length == 0){
            return false
        } else {
            let total = []
            for(let i = 0; i < result.addToCart.length; i++){
                
                total.push(result.addToCart[i].totalAmount * result.addToCart[i].quantity)  
                if(result.addToCart.length - i == 1){
                let newTotal = {
                    subTotal: total.reduce((a, b) => a + b, 0)

                } 
              return  User.findByIdAndUpdate(data.userId, newTotal).then((result , error) => {
                if(error){
                    return false
                } else {
                    return `${newTotal.subTotal}`
                }
              })
                }             
            }
            
            
        }
    })
}

module.exports.checkout =  (data) => {
  return User.findById(data.userId).then(user => {
    if(user.balance >= user.subTotal){
        for(let i = 0; i < user.addToCart.length; i++){
        user.orders.push(user.addToCart[i])
         if(user.addToCart.length - i == 1){
            return user.save().then((result, error) => {
        if(error){
            return false
        } else {
            return user.orders
        }
    })
         }
    }
} else {
    return false
}
   
  })
           
}

module.exports.productCheckout = (data) => {
          return Product.findById(data.productId).then(product => {        
           return User.findById(data.userId).then(user => {
                for(let i = 0; i < user.addToCart.length; i++){
                    console.log(product.id)
                    
                if(product.id == user.addToCart[i].productId){
                    let total = product.price * user.addToCart[i].quantity
                    console.log(user.addToCart[i].productId)
                    console.log(total)
                    product.customersOrder.push({userId: user.id, quantity: user.addToCart[i].quantity, subTotal: total})
                  return product.save().then((product, error) => {
                        console.log(product)
                        if(error){
                            return false
                        } else {
                            return true
                        }
                    })
                } else {
                    continue;
                }
            }
        })
    })
}

module.exports.getBalance = (data) => {
    return User.findById(data.userId).then(result => {
        if(result.balance >= result.subTotal){
            return true
        } else {
            return false
        }
    })
}

module.exports.payment = async (data) => {
    let isBalanceUpdated = await User.findById(data.userId).then(user => {
        let total = data.totalAmount * data.quantity
        if(user.balance >= total){
        let newTotalAmount = (user.balance - total)
        let updatedBalance = {
            balance: newTotalAmount
        }
        console.log(updatedBalance)
        return User.findByIdAndUpdate(data.userId, updatedBalance).then((balance, error) => {
        if(balance == null || balance == undefined){
            return false
        } else {
            return true
        }
    })
        } else {
            return false
        }
})
    if(isBalanceUpdated == true) {
        let isStocksUpdated = await Product.findById(data.productId).then(product => {
        let newStocks = product.stocks - data.quantity
        let updatedStocks = {
            stocks: newStocks
        }
        return Product.findByIdAndUpdate(product.id, updatedStocks).then((stocks, error) => {
          if(stocks == null || stocks == undefined){
            console.log(stocks)
            return false
        } else {
            return true
        }  
        })
})
    } else {
        return false
    }
        if (isBalanceUpdated ) {
        return true
    } else if (isBalanceUpdated == false) {
        return false
    } else {
        false
    }

}

module.exports.emptyCart = (data) => {
    return User.findById(data.userId).then(user => {
         user.addToCart.splice(0)
       return user.save().then((result, error) => {
        if(user.addToCart.length == 0){
            return true
        } else {
            return false
        }
       })
        
    })
}

module.exports.getUserOrder = (data) => {
    
    if(data.userId === data.verifyId || data.isAdmin === false){
        return User.findById(data.verifyId).then(result => {
            if(result.orders.length == 0){
                return false
            } else {
                return result.orders
            }
        })
    } else {
        return User.findById(data.userId).then(result => {
            if(result.orders.length == 0){
                return false
            } else {
                return result.orders
            }
        })
    }
}

module.exports.getAllOrders = () => {
    
  return User.find().then(users => {
         let showOrders = []
         for (let i = 0; i < users.length; i++){
            if (users[i].orders.length != 0){
                showOrders.push({id: users[i].id, email: users[i].email, order: users[i].orders})
                continue
            } else {
                continue
            }
         }

         return showOrders
  }).then(result => {
    return result
  })
}

// .then((updated, error) => {
//         if(balance == null || balance == undefined){
//             return false
//         } else {
//             return true
//         }
//     })