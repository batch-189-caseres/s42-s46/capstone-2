const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require("../auth");


router.post("/", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    productController.addProduct(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})

router.get("/", (req, res) => {

	productController.getAllActive().then(resultFromController => res.send(resultFromController))
})

router.get("/getAllProduct", (req, res) => {

	productController.getAllProduct().then(resultFromController => res.send(resultFromController))
})

router.get("/getAllOnSale", (req, res) => {

	productController.getAllOnSale().then(resultFromController => res.send(resultFromController))
})

router.get("/:productId", (req, res) => {

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))

});

router.put("/:productId", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if(userData.isAdmin){
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("You must be an Admin!")
	}

})

router.put('/:productId/archive', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	
	if(userData.isAdmin) {
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("must be admin!")
	}
})

router.put('/:productId/setOnSale', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	
	if(userData.isAdmin) {
		productController.setOnSale(req.params, req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("must be admin!")
	}
})

router.delete('/:productId/removeProduct', auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)
	
	if(userData.isAdmin) {
		productController.removeProduct(req.params).then(resultFromController => res.send(resultFromController))
	} else {
		res.send("must be admin!")
	}
})





module.exports = router; 