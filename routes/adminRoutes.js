const express = require('express');
const router = express.Router();
const adminController = require('../controllers/adminController');
const auth = require("../auth");

router.post("/register", (req, res) => {
    adminController.createAdmin().then(resultFromController => res.send(resultFromController))
});

router.post("/:userId", (req, res) => {
	let userId = req.params.userId
	adminController.addUser(userId).then(resultFromController => res.send(resultFromController))
})

router.get("/", (req, res) => {

	adminController.getUser().then(resultFromController => res.send(resultFromController))
})






module.exports = router; 