const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../auth");

router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.get("/checkBalance", (req, res) => {
const userData = auth.decode(req.headers.authorization);

    let userId = userData.id

    userController.checkBalance(userId).then(resultFromController => res.send(resultFromController));
})

router.put("/cashIn", (req, res) => {
    const userData = auth.decode(req.headers.authorization);

    let data = {
        userId: userData.id,
        isAdmin: userData.isAdmin
    }

    if(data.isAdmin){
        return 'must be user'
    } else {
        userController.cashIn(data, req.body).then(resultFromController => res.send(resultFromController));
    }
})


router.get("/details", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    userController.getProfile({id : userData.id}).then(resultFromController => res.send(resultFromController))

});

router.get("/:userId/details", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    userController.getAdminProfile({id : req.params.userId}).then(resultFromController => res.send(resultFromController))

});

router.put("/:userId/admin", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin){
    	userController.setAdmin(req.params, req.body).then(resultFromController => res.send(resultFromController))
    } else {
    	res.send("Must be admin!")
    }

});


router.get("/checkCart", auth.verify, (req, res) => {
     const userData = auth.decode(req.headers.authorization)

     if(userData.isAdmin == false){
       userController.checkCart(userData.id).then(resultFromController => res.send(resultFromController))
    } else {
        res.send("Must be user!")
    }
})

router.get("/:productId/itemCheckCart", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    userController.itemCheckCart(userData.id, req.params).then(resultFromController => res.send(resultFromController))
})

router.put("/:productId/addItemQuantity", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    userController.addItemQuantity(userData.id, req.params).then(resultFromController => res.send(resultFromController))
})

router.put("/:productId/updatedQuantity", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    let quantity = req.body.quantity
    userController.updatedQuantity(userData.id, req.params, quantity).then(resultFromController => res.send(resultFromController))
})


router.post("/:productId/addToCart", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    let data = {
        userId: userData.id,
        isAdmin: userData.isAdmin,
        productId: req.params.productId,
        totalAmount: req.body.totalAmount,
        quantity: req.body.quantity
    }
    if(userData.isAdmin == false){
       userController.addToCart(data).then(resultFromController => res.send(resultFromController))
    } else {
        res.send("Must be user!")
    }

})



router.delete("/:productId/removeToCart", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    let data = {
        userId: userData.id,
        isAdmin: userData.isAdmin,
        productId: req.params.productId,
    }
    if(userData.isAdmin == false){
       userController.removeToCart(data).then(resultFromController => res.send(resultFromController))
    } else {
        res.send("Must be user!")
    }

})


router.put("/getTotal", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    let data = {
       userId: userData.id,
       isAdmin: userData.isAdmin,
       balance: userData.balance 
    }
    if(userData.isAdmin == false){
       userController.subtotal(data).then(resultFromController => res.send(resultFromController))
    } else {
        res.send("Must be user!")
    }

}) 


router.get("/getBalance", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
  
    let data = {
        userId: userData.id,
        isAdmin: userData.isAdmin,
        balance: userData.balance
    }
     if(userData.isAdmin == false){
            userController.getBalance(data).then(resultFromController => res.send(resultFromController))
    } else {
        res.send("Must be user!")
    }
})

router.post("/checkout", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
  
    let data = {
        userId: userData.id,
        isAdmin: userData.isAdmin,
        balance: userData.balance
    }
    if(userData.isAdmin == false){
            userController.checkout(data).then(resultFromController => res.send(resultFromController))
    } else {
        res.send("Must be user!")
    }
});

router.post("/:productId/checkout", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
  
    let data = {
        userId: userData.id,
        isAdmin: userData.isAdmin,
        balance: userData.balance,
        productId: req.params.productId
        
    }
    if(userData.isAdmin == false){
            userController.productCheckout(data).then(resultFromController => res.send(resultFromController))
    } else {
        res.send("Must be user!")
    }
});


router.put("/:productId/checkout", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    let data = {
        userId: userData.id,
        isAdmin: userData.isAdmin,
        balance: userData.balance,
        productId: req.params.productId,
        quantity: req.body.quantity,
        totalAmount: req.body.totalAmount
    }
     if(userData.isAdmin == false){
        if(data.balance >= req.body.totalAmount ) {
            userController.payment(data).then(resultFromController => res.send(resultFromController))
        } else {
             res.send("insuficient balance!")
        }
    } else {
        res.send("Must be user!")
    }
})

router.delete("/emptyCart", auth.verify, (req, res) => {
     const userData = auth.decode(req.headers.authorization)
     let data = {
        userId: userData.id
    }
    
    userController.emptyCart(data).then(resultFromController => res.send(resultFromController))
})


router.get("/:userId/getOrder", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)

    let data = {
        verifyId: userData.id,
        isAdmin: userData.isAdmin,
        userId: req.params.userId
    }
    
    userController.getUserOrder(data).then(resultFromController => res.send(resultFromController))
    
    
});

router.get("/getAllOrder", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    
    if(userData.isAdmin == true){
        userController.getAllOrders().then(resultFromController => res.send(resultFromController))
    } else {
        res.send("Must be admin!")
    }
    
});




module.exports = router;


