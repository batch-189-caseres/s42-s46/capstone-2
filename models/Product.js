const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Product is required"]
	},
	description: {
		type: String,
	},
	category: {
		type: String,
		required: [true, "Category is required"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	price: { 
		type: Number,
		required: [true, "Price is required"]
	},
	stocks: {
		type: Number,
		required: [true, "Stocks is required"]
	},
	onSale: {
		type: Boolean,
		default: false
	},
	discount: {
		type: Number,
		default: 1
	},
	imgUrl: {
		type: String,
		required: [true, "Image Url is required"]
	},
	createdOn: {
		type: Date,
		default: new Date()
	},

	customersOrder: [
		{
			userId: {
				type: String,
				required: [true, "UserID is required"]
			},
			quantity: {
				type: Number,
				required: [true, "No. of items per product required"]
			},
			subTotal: {
				type: Number,
				required: [true, "Total Amount must be computed"]
			},
			orderedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
})


module.exports = mongoose.model("Product", productSchema);
