const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "First name is required"]
	},
	lastName : {
		type : String,
		required : [true, "Last name is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String, 
		required : [true, "Mobile No is required"]
	},

	balance : {
		type : Number,
		default : 0
	},
	addToCart : [
		{
			productId: {
				type: String,
				required: [true, "ProductID is required"]
			},
			name: {
				type: String,
				required: [false, "Name is required"]
			},
			description: {
				type: String,
				required: [false, "description is required"]
			},
			imgUrl: {
				type: String,
				required: [false, "Image URl is required"]
			},
			category: {
				type: String,
				required: [false, "Category is required"]
			},
			quantity: {
				type: Number,
				required: [true, "No. of items per product required"]
			},
			totalAmount: {
				type: Number,
				required: [true, "Total Amount must be computed"]
			}

		}
	],
	orders : [
		{
			productId: {
				type: String,
				required: [true, "ProductID is required"]
			},
			name: {
				type: String,
				required: [true, "Name is required"]
			},
			description: {
				type: String,
				required: [true, "description is required"]
			},
			imgUrl: {
				type: String,
				required: [true, "Image URl is required"]
			},
			category: {
				type: String,
				required: [true, "Category is required"]
			},
			quantity: {
				type: Number,
				required: [true, "No. of items per product required"]
			},
			totalAmount: {
				type: Number,
				required: [true, "Total Amount must be computed"]
			},
			status: {
				type: String,
				default: "pending"
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}

	],
	subTotal: {
				type: Number,
				default: 0
			}
})

module.exports = mongoose.model("User", userSchema);

